A poorly written, barely functionnal Memory Pak Dumper for N64 using an Arduino.

# What do I need?

An Arduino, an N64 official controller (it's untested on third parties but given the way N64 controllers works, it might actually work), 3 cables and a resistor.


# What do I get from this program?

The program will let you read a complete N64 Memory Pak plugged into a controller, plugged into an Arduino and write that same dump into a N64 Memory Pak.

This is, at the moment, not compatible with other N64 Memory Pak format used by emulators like Mupen64+ (but I firmly intend to make it compatible as soon as possible).

# Plugging to an Arduino

You must plug the GND of the controller to the GND of the Arduino. Port 3.3V to port 3.3V of the Arduino.

Signal is transmitted through port 9 of the Arduino (and the corresponding port on the controller). You will also need to plug a 1Kohms resistor between the signal port and the 3.3V port.

# How the controller works exactly?

## Controller design

There are 3 pins on the controller:

![N64 pinout](/img/N64_schema.jpg)

Transmitting signal to the controller is a bit cumbersome: to transmit 1, you put the signal line to LOW for 1µs and HIGH for 3μs; to transmit 0, you put the signal line to HIGH for 1µs then LOW for 3µs.

![N64 signal](/img/n64bits.gif)

Receiving data works pretty much the same way.

## Signals handling

The controller received instructions followed by a stop bit. It responds by a stop bit 2µs later and then is unavailable for 200µs. The instructions are as follows:
* 0x00 initialize the controller
* 0x01 asks for the controller button status: it responds with 16 bits for button status then 16 bits for stick status (X and Y)
* 0x02 triggers a read on the controller slot (more on that later)
* 0x03 triggers a write on the controller slot (more on that later)

For the read signal (0x02), the next 2 bytes correspond to a command:
* 0x0000 to 0x7FFF is the 32KiB of the memory card
* 0x8000 for initialization and identification
* 0xC01B controls the rumble pak

Write signal (0x03) on 0x0000 to 0x7FFF actually writes to the memory card (on a 32 bytes boundary). Writing to 0x8001 just after a write to the memory card let you check the CRC of the last 32 bytes written.

## CRC

There are 2 CRC for the packet sent: 1 for data and 1 for the address.

So if you want to read data from 0x0020 (second 32 bytes) on the controller slot, you must do the following in that precise order (and with the right timing):
* send 0x02 to the controller
* calculate the addres CRC -> 0x0015
* add the read address with the CRC -> 0x0035 (= 0x0020 + 0x0015)
* send 0x0035 to the controller
* read 32 bytes of data
* read 1 byte of data CRC
